<?php

use Gregwar\Image\Image;

$app->match('/', function() use ($app) {
    return $app['twig']->render('home.html.twig');
})->bind('home');

$app->match('/books', function() use ($app) {
    return $app['twig']->render('books.html.twig', array(
        'books' => $app['model']->getBooks()
    ));
})->bind('books');

$app->match('/admin', function() use ($app) {
    $request = $app['request'];
    $success = false;
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('login') && $post->has('password') &&
            in_array(array($post->get('login'), $post->get('password')),$app['config']['admin'])) {
            $app['session']->set('admin', true);
            $success = true;
        }
    }
    if($success)
        return $app['twig']->render('home.html.twig', array(
        'success' => $success
    ));
    return $app['twig']->render('admin.html.twig', array(
        'success' => $success
    ));
})->bind('admin');

$app->match('/logout', function() use ($app) {
    $app['session']->remove('admin');
    return $app->redirect($app['url_generator']->generate('admin'));
})->bind('logout');

$app->match('/addBook', function() use ($app) {
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }

    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('title') && $post->has('author') && $post->has('synopsis') &&
            $post->has('copies')) {
            $files = $request->files;
            $image = '';

            // Resizing image
            if ($files->has('image') && $files->get('image')) {
                $image = sha1(mt_rand().time());
                Image::open($files->get('image')->getPathName())
                    ->resize(240, 300)
                    ->save('uploads/'.$image.'.jpg');
                Image::open($files->get('image')->getPathName())
                    ->resize(120, 150)
                    ->save('uploads/'.$image.'_small.jpg');
            }

            // Saving the book to database
            $app['model']->insertBook($post->get('title'), $post->get('author'), $post->get('synopsis'),
                $image, (int)$post->get('copies'));
        }
    }

    return $app['twig']->render('addBook.html.twig');
})->bind('addBook');

$app->match('fiche/{id}', function($id) use ($app) {
    $book = $app['model']->getBook($id);
    $exemplaires = $app['model']->getExemplaire($id);
    $emprunts = $app['model']->getEmpruntsNonFini();
    $nbExemplairesDispo = $app['model']->getNbExemplairesDisponible($id);
    return $app['twig']->render('fiche.html.twig', array(
        'book' => $book,
        'exemplaires' => $exemplaires,
        'emprunts' => $emprunts,
        'nbExemplairesDispo' => $nbExemplairesDispo 
    ));
})->bind('fiche');



//emprunter livre
$app->match('addEmprunt/{id}', function($id) use ($app) {
    $success = false;
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }
    $request = $app['request'];
    if ($request->getMethod() == 'POST') {
        $post = $request->request;
        if ($post->has('personne') && $post->has('date')) {
            $today = new DateTime();
            $dateFin = new DateTime($post->get('date'));
            if($dateFin > $today){
                // Saving the emprunt to database
                $app['model']->insertEmprunt($id, $post->get('personne'), $post->get('date'));
                $success = true;
           }
        }
    }
        return $app['twig']->render('emprunt.html.twig', array(
            'success' => $success 
            ));
})->bind('addEmprunt');

//retour
$app->match('retour/{id}', function($id) use ($app) {
    $success = false;
    if (!$app['session']->has('admin')) {
        return $app['twig']->render('shouldBeAdmin.html.twig');
    }
    $retour = $app['model']->setEmprunt($id);    
    return $app['twig']->render('home.html.twig') ;
})->bind('retour');