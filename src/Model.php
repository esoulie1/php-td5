<?php

class Model
{
    protected $pdo;

    public function __construct(array $config)
    {
        try {
            if ($config['engine'] == 'mysql') {
                $this->pdo = new \PDO(
                    'mysql:dbname='.$config['database'].';host='.$config['host'],
                    $config['user'],
                    $config['password']
                );
                $this->pdo->exec('SET CHARSET UTF8');
            } else {
                $this->pdo = new \PDO(
                    'sqlite:'.$config['file']
                );
            }
        } catch (\PDOException $error) {
            throw new ModelException('Unable to connect to database');
        }
    }

    /**
     * Tries to execute a statement, throw an explicit exception on failure
     */
    protected function execute(\PDOStatement $query, array $variables = array())
    {
        if (!$query->execute($variables)) {
            $errors = $query->errorInfo();
            throw new ModelException($errors[2]);
        }

        return $query;
    }

    /**
     * Inserting a book in the database
     */
    public function insertBook($title, $author, $synopsis, $image, $copies)
    {
        $query = $this->pdo->prepare('INSERT INTO livres (titre, auteur, synopsis, image)
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($title, $author, $synopsis, $image));

        // TODO: Créer $copies exemplaires

        $bookid = $this->pdo->lastInsertId();
        for ($exemplaires=0; $exemplaires < $copies; $exemplaires++) 
        { 
            $query = $this->pdo->prepare('INSERT INTO exemplaires (book_id) VALUES (?)');
            $this->execute($query, array($bookid));
        }
        

    }

    public function insertEmprunt($idExemplaire, $personne, $dateFin)
    {
        $today = date("Y/m/d");
        $query = $this->pdo->prepare('INSERT INTO emprunts (personne, exemplaire, debut, fin) 
            VALUES (?, ?, ?, ?)');
        $this->execute($query, array($personne, $idExemplaire, $today, $dateFin));
    }

    /**
     * Getting all the books
     */
    public function getBooks()
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres');

        $this->execute($query);

        return $query->fetchAll();
    }

    /**
     * Getting book by id
     */
    public function getBook($id)
    {
        $query = $this->pdo->prepare('SELECT livres.* FROM livres WHERE id = ?');
        $query->execute(array($id));

        return $query->fetch();
    }

    public function getExemplaires()
    {
        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires');
        $this->execute($query);
        return $query->fetchAll();
    }

    public function getExemplaire($bookid)
    {
        $query = $this->pdo->prepare('SELECT exemplaires.* FROM exemplaires WHERE book_id = ?');
        $query->execute(array($bookid));

        return $query->fetchAll();
    }

    public function getEmpruntsNonFini()
    {
        $query = $this->pdo->prepare('SELECT emprunts.* FROM emprunts WHERE fini = 0');
        $this->execute($query);

        return $query->fetchAll();
    }

    public function setEmprunt($id)
    {
        $query = $this->pdo->prepare('UPDATE emprunts SET fini = 1 WHERE id = ?');
        $query->execute(array($id));
    }
    
    public function getNbExemplairesDisponible($bookid)
    {
        $query = $this->pdo->prepare('SELECT * FROM exemplaires 
            WHERE ( SELECT COUNT(*) FROM emprunts 
            WHERE exemplaire = exemplaires.id AND fini = 0) = 0
            AND exemplaires.book_id = ?'
        );
        $query->execute(array($bookid));
        
        return count($query->fetchAll());
    }

}
